import 'package:file1m2/Login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Login()))
        },
        child: const Text('Home'),
      ),
    );
  }
}
