import 'package:file1m2/Dashboard.dart';
import 'package:file1m2/Register.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purpleAccent,
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            Text("Login"),
            TextField(
                decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter your emain',
            )),
            Text("Register"),
            TextField(
                decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter your password',
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Register()))
        },
        backgroundColor: Colors.cyan,
        child: const Text('Register'),
      ),
    );
  }
}
