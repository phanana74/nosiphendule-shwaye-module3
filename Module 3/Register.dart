import 'package:file1m2/Dashboard.dart';
import 'package:file1m2/Login.dart';
import 'package:file1m2/SignUp.dart';
import 'package:flutter/material.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SignUp'),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SignUp()),
          )
        },
        child: const Text('Sing Up'),
      ),
    );
  }
}
