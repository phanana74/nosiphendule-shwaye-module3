import 'package:file1m2/Dashboard.dart';
import 'package:flutter/material.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register here'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: const [
          Text("Email"),
          TextField(
              decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Enter your emain',
          )),
          Text("Create password"),
          TextField(
              decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Enter your password',
          )),
          Text("Confirm password"),
          TextField(
              decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Enter your password',
          ))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: ((context) => Dashboard())))
        },
      ),
    );
  }
}
