import 'package:file1m2/Login.dart';
import 'package:file1m2/Register.dart';
import 'package:file1m2/SignUp.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Login());
  }
}
